# Pihole-Stubby-DNS

## Description
A helm Chart for a DNS stack
Block adds while keeping you browsing private

This DNS Stack comprises 2 images.
- [pihole](https://pi-hole.net/) - ad blocker
- [stubby](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Daemon+-+Stubby) - tls dns resolver

## Requirements
- a kubernetes cluster (it could be [k3s](https://k3s.io/))
- kubectl configured to hit that cluster
- [helm](https://helm.sh/) client installed (optional)
- helm tiller initialised in the k8s cluster (optional)
- [MetalLB deployed](https://metallb.universe.tf/installation/) (optional)

## Installation

The images used in this projects support both archs, amd64 and amrv7

### Before installation
This deployment is currently using `local path volumes` to hold persistent data
make sure you have the following directories in you system

- /home/pi/pihole/pihole
- /home/pi/pihole/dnsmasq.d

Or you can update them on the values.yaml file or override them in the cli with

	helm install -n <release name> --set volumes.piholeetc=/path/to/pihole/confdir,volumes.dnsmasqetc=/path/to/dnsmasq.d/confdir iotops/pihole-stubby-dns

### Tillerless deployment

This deployment method is only supported on a k8s cluster with a helm operator already installed, like k3s.

If this is not the case, try the deployment **Using helm repo**.

	kubectl apply -f https://gitlab.com/iotops/helmcharts/pihole-stubby-dns/raw/master/assets/manifests/pihole.yaml

### Using helm repo

This is an alternative to the previous deployment method. For this approach you will need **helm client** and **tiller** deployed on your system and cluster respectively 

	helm repo add iotops https://iotops.gitlab.io/charts
	helm repo update
	helm install -n <release name> iotops/pihole-stubby-dns
	
### From git
Clone this repo. Then run

	helm install -n <release name> . 

## Check status

By default all resources are deployed on a new namespace called **dns**. If you used another namespace, just replace the dns with the one you used.

	kubectl get pods,svc -n dns

## Uninstall

If you followed the tiller less approach with the helm operator.

Run

	kubectl delete -f https://gitlab.com/iotops/helmcharts/pihole-stubby-dns/raw/master/assets/manifests/pihole.yaml

If it was deployed using helm, then run

	helm delete --purge <release name>

## Configurations
Variables to customize.

- .Values.image.pihole.env.webpass.
- .Values.service.port
- .Values.volumes.piholeetc
- .Values.volumes.dnsmasqetc

If ussing [MetalLB](https://metallb.universe.tf/)

- .Values.services.loadBalancerIP


[![](https://gitlab.com/iotops/charts/raw/master/sponsored-by-3xm-group.png)](http://www.3xmgroup.com/)

