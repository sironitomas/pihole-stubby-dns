# Change log

- 0.5.2
  - Removed readyness and liveness tests from stubby container since this one depends a lot on the network health and not solely on the containerised app
- 0.5.1
  - Fixed issue of stubby not listening
- 0.5.0
  - Implemented multi container pods
- 0.4.0
  - Added limited service account for pods
